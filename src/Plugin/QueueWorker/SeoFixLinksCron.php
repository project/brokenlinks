<?php

namespace Drupal\brokenlinks\Plugin\QueueWorker;

/**
 * Processes Tasks for Broken Links Fixer.
 *
 * @QueueWorker(
 *   id = "brokenlinks_shortener",
 *   title = @Translation("Fix internal/external links."),
 *   cron = {"time" = 30}
 * )
 */
class SeoFixLinksCron extends SeoFixLinksBase {}
