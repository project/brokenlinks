<?php

namespace Drupal\brokenlinks\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\brokenlinks\SeoBrokenLinksShorten;
use Drupal\field\Entity\FieldConfig;

/**
 * Provides base functionality for the BrokenLinksFixer Queue Workers.
 */
abstract class SeoFixLinksBase extends QueueWorkerBase implements
    ContainerFactoryPluginInterface {

  const FORMATTED_TEXTS = [
    'text_with_summary',
    'text',
    'text_long',
  ];

  /**
   * ShortenUrls service.
   *
   * @var \Drupal\brokenlinks\SeoBrokenLinksShorten
   */
  protected $shortenService;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SeoFixLinksBase object.
   *
   * @param \Drupal\brokenlinks\SeoBrokenLinksShorten $short_service
   *   ShortenUrls service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(
    SeoBrokenLinksShorten $short_service,
    EntityTypeManagerInterface $entity_manager) {

    $this->shortenService = $short_service;
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {

    return new static(
      $container->get('brokenlinks.shorten_url'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $shorten = $this->shortenService;
    $entity = $this->entityTypeManager->getStorage($data->type)
      ->load($data->id);
    $shortUrls = [];

    // Get each Formatted text fields from the entities.
    foreach ($entity->getFieldDefinitions() as $key => $field) {
      if ($field instanceof FieldConfig &&
          in_array($field->getType(), static::FORMATTED_TEXTS)) {

        // Retrieve value from the field.
        if ($entity->hasField($key)) {
          $bodyField = $entity->get($key)->value;
          $format = $entity->get($key)->format;

          // Parse each broken url and create a shorten url for it.
          $broken_urls = $shorten->getBrokenUrls($bodyField);

          if (!empty($broken_urls)) {
            foreach ($broken_urls as $longUrl) {
              $shortUrls[$longUrl] = $shorten->googlShorten($longUrl);
            }

            $shortUrls = array_filter($shortUrls);

            if (!empty($shortUrls)) {
              // Replace body broken links with updated ones.
              $bodyField = str_replace(
                array_keys($shortUrls),
                array_values($shortUrls),
                $bodyField
              );

              $entity->{$key}->setValue(
                ['value' => $bodyField, 'format' => $format]
              );

              return $entity->save();
            }
          }
        }
      }
    }

    return FALSE;
  }

}
