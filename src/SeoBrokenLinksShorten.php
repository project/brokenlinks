<?php

namespace Drupal\brokenlinks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SeoBrokenLinksShorten.
 *
 * @package Drupal\brokenlinks
 */
class SeoBrokenLinksShorten {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The core logger channel interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The symfony request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Constructs a SeoBrokenLinksShorten object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The core logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The sympfony request stack.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    RequestStack $request) {

    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('brokenlinks');
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Shorten URL.
   *
   * @param string $longUrl
   *   The url that needs to be shortened.
   */
  public function googlShorten($longUrl) {
    // Load google shortener functions.
    // Load config values.
    $config = $this->configFactory->get('brokenlinks.settings');
    // Define default variables.
    $access_token = $config->get('access_token');
    $google = new GoogleShortener($access_token);
    $result = $google->create([
      'longUrl' => $longUrl,
    ]);

    $data = json_decode($result);
    if (!empty($data->id)) {
      $this->logger->info($this->t(
        'Successfully converted @long to @short',
        ['@long' => $longUrl, '@short' => $data->id]
      ));

      return $data->id;
    }

    $this->logger->info($this->t(
      'Could not convert "@long" : @error',
      ['@long' => $longUrl, '@error' => $data->error->message]
    ));

    return FALSE;
  }

  /**
   * Function to optain broken url from a body field.
   *
   * @param string $body
   *   Node body value.
   */
  public function getBrokenUrls($body) {
    if (!empty($body)) {
      $bodyurls = $this->getUrls($body);
      $hrefurl = $this->getHrefUrls($body);

      // Get links from body text.
      $urls = array_unique(array_merge($bodyurls, $hrefurl));

      foreach ($urls as $url) {
        // Add base_url if not already added.
        $url = $this->addBaseUrl($url);

        try {
          // Google Shorte requires SSL peer verification, which may prevent
          // out of date servers from successfully processing API requests. If
          // you get an error related to peer verification, you may need to:
          // * Update libraries like openssl and libcurl.
          // * Use https://github.com/paragonie/certainty to get updated
          // certificates.
          // * Update your php.ini to point to your CA certificate bundle with
          // the curl.cainfo setting.
          // * Download the CA certificate bundle file from
          // http://curl.haxx.se/docs/caextract.html, place it in a safe
          // location on your web server, and update your settings.php to set
          // the commerce_authnet_cacert variable to contain the absolute path
          // of the file.
          $handle = curl_init($url);
          curl_setopt($handle, CURLOPT_VERBOSE, TRUE);
          curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, TRUE);
          $content = curl_exec($handle);
          $response = curl_getinfo($handle);
          $status = $response['http_code'];
          $correct_response = [200, 301, 302];

          if ($content && !(in_array($status, $correct_response))) {
            $brokenUrls[] = $url;
          }
        }
        catch (RequestException $e) {
          return FALSE;
        }
      }

      if (!empty($brokenUrls)) {
        return $brokenUrls;
      }
    }
  }

  /**
   * Check body field text to find for urls and converts them into absolute url.
   *
   * @param string $string
   *   Contains body field strings $string.
   *
   * @return string
   *   Absolute url.
   */
  public function getUrls($string) {
    $regex = '/https?\:\/\/[^\" ]+/i';
    preg_match_all($regex, $string, $matches);

    return ($matches[0]);
  }

  /**
   * Check body field to find ahref and then convert them into absolute urls.
   *
   * @param string $string
   *   Contains body field strings $string.
   *
   * @return string
   *   Absolute url string.
   */
  public function getHrefUrls($string) {
    $regex = '/href=["\']?([^"\'>]+)["\']?/';
    preg_match_all($regex, $string, $matches);

    return ($matches[1]);
  }

  /**
   * Check absolute urls, if not found then converts them into absolute url.
   *
   * @param string $url
   *   Contains url string $url.
   *
   * @return string
   *   Absolute url.
   */
  public function addBaseUrl($url) {
    $base_url = $this->request->getCurrentRequest()->getSchemeAndHttpHost();

    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
      $url = $base_url . $url;
    }

    return $url;
  }

}
