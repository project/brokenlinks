<?php

namespace Drupal\brokenlinks;

use Drupal\Core\Queue\SuspendQueueException;

/**
 * Batch controller to process a queue from the UI.
 *
 * Class SeoFixBrokenLinkBatch.
 *
 * @package Drupal\brokenlinks
 */
class SeoFixBrokenLinkBatch {

  /**
   * Batch step definition to process one queue item.
   *
   * Based on \Drupal\Core\Cron::processQueues().
   */
  public static function process($queue_name, $context) {
    if (isset($context['interrupted']) && $context['interrupted']) {
      return;
    }

    /* @var $queueManager \Drupal\Core\Queue\QueueWorkerManagerInterface */
    $queueManager = \Drupal::service('plugin.manager.queue_worker');
    /* @var \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queueFactory = \Drupal::service('queue');

    $info = $queueManager->getDefinition($queue_name);
    $title = $info['title'];

    $queueFactory->get($queue_name)->createQueue();

    $queue_worker = $queueManager->createInstance($queue_name);
    $queue = $queueFactory->get($queue_name);

    if ($item = $queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);

        $context['message'] = $title;
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        // If the worker indicates there is a problem with the whole queue,
        // release the item and skip to the next queue.
        $queue->releaseItem($item);

        \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('cron'), $e), fn() => watchdog_exception('cron', $e));

        // Skip to the next queue.
        $context['interrupted'] = TRUE;
      }
      catch (\Exception $e) {
        // In case of any other kind of exception, log it and leave the item
        // in the queue to be processed again later.
        \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('cron'), $e), fn() => watchdog_exception('cron', $e));
      }
    }
  }

}
