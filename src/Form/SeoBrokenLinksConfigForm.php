<?php

namespace Drupal\brokenlinks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configuration form for SeoBrokenLinksConfigForm.
 */
class SeoBrokenLinksConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brokenlinks_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'brokenlinks.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('brokenlinks.settings');

    $form['seo_bl'] = [
      '#type' => 'container',
      '#title' => $this->t('Google API GENERIC ACCESS TOKEN'),
    ];

    $form['seo_bl']['googl_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Generic Access Token'),
      '#default_value' => $config->get('access_token'),
      '#required' => TRUE,
      '#description' => $this->t(
        'Register the Project and enable for this the @google Service.',
        [
          '@google' => Link::fromTextAndUrl(
            $this->t('Google Shortener'),
            Url::fromUri(
              'https://console.developers.google.com/apis/library/urlshortener.googleapis.com'
            )
          )->toString(),
        ]
      ),
    ];

    $form['seo_bl']['node_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Activate google shortener converter upon node submit?'
      ),
      '#default_value' => $config->get('node_submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('brokenlinks.settings');
    $values = $form_state->getValues();

    $config->set('access_token', $values['googl_access_token']);
    $config->set('node_submit', $values['node_submit']);
    $config->save();

    return parent::submitForm($form, $form_state);
  }

}
