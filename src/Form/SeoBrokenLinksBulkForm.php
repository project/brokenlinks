<?php

namespace Drupal\brokenlinks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\EntityTypeBundleInfo;

/**
 * Provides a custom form for SeoBrokenLinksBulkForm.
 */
class SeoBrokenLinksBulkForm extends FormBase {
  const BUNDLE_MAP = [
    'block_content' => ['block_content' => 'type'],
    'comment' => ['comment' => 'comment_type'],
    'file' => ['file_managed' => ''],
    'node' => ['node' => 'type'],
    'shortcut' => ['shortcut' => 'shortcut_set'],
    'taxonomy_term' => ['taxonomy_term' => 'vid'],
    'user' => ['user' => ''],
    'menu_link_content' => ['menu_link_content' => 'bundle'],
  ];

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The QueueFactory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The QueueWorkerManagerInterface.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfo bundles info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * Constructs a new SeoBrokenLinksBulkForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundle_info
   *   Bundle info service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_manager,
    QueueFactory $queue,
    QueueWorkerManagerInterface $queue_manager,
    MessengerInterface $messenger,
    ConfigFactoryInterface $config_factory,
    EntityTypeBundleInfo $bundle_info) {

    $this->entityTypeManager = $entity_manager;
    $this->queueFactory = $queue;
    $this->queueManager = $queue_manager;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brokenlinks_bulk_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['c_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t(
        'Choose Content Types which nodes you want to parse and fix the broken
        links.'
      ),
    ];

    $entityDefinitions = $this->entityTypeManager->getDefinitions();
    // Generate an options list of Entity types.
    foreach ($entityDefinitions as $entityName => $entityDefinition) {
      if ($entityDefinition instanceof ContentEntityType) {
        $options = [];
        $bundles = $this->bundleInfo->getBundleInfo($entityName);

        foreach ($bundles as $key => $bundle) {
          if ($bundle['label'] instanceof TranslatableMarkup) {
            $label = $bundle['label'];
          }
          else {
            $label = $this->t('@bundle', ['@bundle' => $bundle['label']]);
          }

          $options[$key] = $label;
        }

        $form['c_types'][$entityName] = [
          '#title' => $entityDefinition->getLabel(),
          '#type' => 'checkboxes',
          '#options' => $options,
        ];
      }
    }

    $form['fix_now'] = [
      '#type' => 'submit',
      '#name' => 'fix_now',
      '#value' => $this->t('Fix Now (Batch operation)'),
    ];

    $form['fix_later'] = [
      '#type' => 'submit',
      '#name' => 'fix_later',
      '#value' => $this->t('Fix Later (Add to Queue)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Set error to form to not let users create batch with 0 items.
    $values = $form_state->getValues();
    $entityDefinitions = $this->entityTypeManager->getDefinitions();
    $selected = FALSE;
    foreach ($entityDefinitions as $entityName => $entityDefinition) {
      if (isset($values[$entityName])) {
        foreach ($values[$entityName] as $value) {
          if ($value !== 0) {
            $selected = TRUE;
          }
        }
      }
    }

    if (!$selected) {
      $form_state->setErrorByName(
        'c_types',
        $this->t('At least one content type bundle must be selected.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get submit button.
    $handler = $form_state->getTriggeringElement();
    $types = array_filter($form_state->getValue('types'));

    $values = $form_state->getValues();
    $entityDefinitions = $this->entityTypeManager->getDefinitions();
    $content_ids = [];

    // Creates an array with submitted content types ids.
    foreach ($entityDefinitions as $entityName => $entityDefinition) {
      if (isset($values[$entityName])) {
        $content_ids[$entityName] = [];

        foreach ($values[$entityName] as $key => $value) {
          if ($value !== 0) {

            $bundle_schema = static::BUNDLE_MAP[$entityName][key(
              static::BUNDLE_MAP[$entityName]
            )];

            $query = $this->entityTypeManager
              ->getStorage($entityName)
              ->getQuery();

            if ($bundle_schema) {
              $query->condition($bundle_schema, $key);
            }

            $query->accessCheck(FALSE);

            $results = $query->execute();

            $content_ids[$entityName] = array_merge(
              $content_ids[$entityName],
              array_values($results)
            );
          }
        }
      }
    }

    // Remove empty values from array.
    $content_ids = array_filter($content_ids);

    $queue = $this->queueFactory->get('brokenlinks_shortener');

    // Creates items for Queue.
    foreach ($content_ids as $type => $content_id) {
      foreach ($content_id as $value) {
        $item = new \stdClass();
        $item->type = $type;
        $item->id = $value;

        $queue->createItem($item);
      }
    }

    // Process the queue items immediatly using batch API.
    if ($handler['#name'] == 'fix_now') {
      $batch = [
        'operations' => [],
      ];

      if ($queue->numberOfItems()) {
        for ($i = 1; $i < $queue->numberOfItems(); $i++) {
          $batch['operations'][] = [
            '\Drupal\brokenlinks\SeoFixBrokenLinkBatch::process',
            ['brokenlinks_shortener'],
          ];
        }
      }

      batch_set($batch);
    }

    // Add the nodes to queue to process the later.
    if ($handler['#name'] == 'fix_later') {
      $count = $queue->numberOfItems();

      $this->messenger->addStatus($this->t(
        '@count nodes of @type type have been added to queue.',
        [
          '@type' => implode(', ', $types),
          '@count' => $count,
        ]
      ));
    }
  }

}
