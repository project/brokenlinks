<?php

namespace Drupal\brokenlinks\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeForm;
use Drupal\brokenlinks\Plugin\QueueWorker\SeoFixLinksBase;
use Drupal\field\Entity\FieldConfig;

/**
 * Form handler Class for the NodeForm.
 */
class SeoBrokenLinksNodeEditForm extends NodeForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $shorten = \Drupal::service('brokenlinks.shorten_url');
    $entity = $form_state->getFormObject()->getEntity();

    // Get each Formatted text fields from the entities.
    foreach ($entity->getFieldDefinitions() as $key => $field) {
      if ($field instanceof FieldConfig &&
          in_array($field->getType(), SeoFixLinksBase::FORMATTED_TEXTS)) {

        // Retrieve form body value.
        $bodyField = $form_state->getValue($key)[0]['value'];

        // Parse each broken url and create a shorten url for it.
        if ($urls = $shorten->getBrokenUrls($bodyField)) {
          foreach ($urls as $longUrl) {
            $shortUrls[$longUrl] = $shorten->googlShorten($longUrl);
          }

          // Replace body broken links with updated ones.
          if (!empty($shortUrls)) {
            $shortUrls = array_filter($shortUrls);
            $bodyField = str_replace(
              array_keys($shortUrls),
              array_values($shortUrls),
              $bodyField
            );

            $format = $this->entity->get($key)->format;

            $this->entity->{$key}->setValue(
              ['value' => $bodyField, 'format' => $format]
            );
          }
        }
      }
    }

    parent::save($form, $form_state);
  }

}
