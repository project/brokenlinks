<?php

namespace Drupal\brokenlinks;

/**
 * Class GoogleShortener.
 */
class GoogleShortener {

  private $endpoint;
  private $apiKey;
  private $ch;
  private $params;

  /**
   * Constructor for GoogleShortener class.
   */
  public function __construct(
    $api_key,
    $endpoint = 'https://www.googleapis.com/urlshortener/v1/url') {

    $this->apiKey = $api_key;
    $this->endpoint = $endpoint;
  }

  /**
   * Create short url (goo.gl) from url.
   *
   * @param array $data
   *   Data parameter.
   *
   * @return stringJSON
   *   stdClass if object object param is set to true.
   */
  public function create(array $data = []) {
    $this->ch = curl_init($this->endpoint . "?key=" . $this->apiKey);
    if (!array_key_exists('object', $data)) {
      $data['object'] = FALSE;
    }

    $this->params = ['longUrl' => $data['longUrl']];
    $result = $this->sendRequest();

    return ($data['object']) ? json_decode($result) : $result;
  }

  /**
   * Show original url from short url (goo.gl).
   *
   * @param array $data
   *   Data parameter.
   *
   * @return stringJSON
   *   stdClass if object param is set to true.
   */
  public function expand(array $data = []) {
    $shortUrl = 'shortUrl=' . $data['shortUrl'];

    if (isset($data['projection']) && !is_null($data['projection'])) {
      $projection = '&projection=' . $data['projection'];
    }
    else {
      $projection = NULL;
    }

    $requestUrl = $this->endpoint . "?key=" . $this->apiKey . '&' . $shortUrl;
    $requestUrl .= $projection;

    if (!array_key_exists('object', $data)) {
      $data['object'] = FALSE;
    }

    $this->ch = curl_init($requestUrl);
    $result = $this->sendRequest();

    return ($data['object']) ? json_decode($result) : $result;
  }

  /**
   * Send request using curl.
   *
   * @return stringJSON
   *   stdClass if object param is set to true.
   */
  private function sendRequest() {
    curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($this->ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
    ]);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

    if (count($this->params) > 0) {
      curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($this->params));
    }

    $result = curl_exec($this->ch);
    curl_close($this->ch);

    return $result;
  }

}
