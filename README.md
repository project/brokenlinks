INTRODUCTION
------------
# SEO Broken Links


REQUIREMENTS
------------
No additional requirements.


RECOMMENDED MODULES
-------------------
No modules required.


INSTALLATION
------------
- Install the SEO Broken Links module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
-------------

 - Follow the path "/admin/config/services/seo-broken-links-settings".
 - Add the Google Access token key & check the "Activate google shortener
 converter upon node submit?" in order to make it work on node submit.
 - Follow the path "/admin/config/services/seo-broken-links-bulk" to fix
 manually the links from the other content entities those have Formatted Text
 fields.

TROUBLESHOOTING
---------------
 Q: The Worker does not fix the links.
 A: Please make sure you ahve added the correct Google Access token key on the
   "/admin/config/services/seo-broken-links-settings:.

MAINTAINERS
-----------
  * Nicolae Procopan (thebumik) - https://www.drupal.org/u/thebumik
  * Lilian Catanoi (liliancatanoi90) - https://www.drupal.org/u/liliancatanoi90

This project is sponsored by:
 * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web
  development company based in Toronto. In the past we provided Drupal
  development solutions for a variety of Canadian and foregin companies with
  outstanding results.
